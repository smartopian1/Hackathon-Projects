Consent as the Future Commerce : CISWG

Calling everyone who cares about consent to get involved in this event.   We are looking for writers/researchers, technologist and every person who is sick with being surveillance without consent to show up with passion. 

The idea is to link consent and its role in information sharing, autonomy, law, society, discussing:   
Consent Based Networking: User Submitted Terms
attacks on consent
defining for consent
lobbying for consent
Practical tools for people and business
Consent for Business 
Customer Commons: New tools for managing consent and companies

Themes

How consent can be used to control personal data? 
How can consent be used to increase experience, personally and as a customer? 

We have two main project streams. 

Company Facing: consent receipt project to provide consent receipts to companies who want to try them out
People Facing: Privacy Day Project - Demonstrate How Consent is future of Conference Commerce with physics

We need all sort of skills to help with this project and just a small contribution will go along way. 


Consent for Business and Consent for Privacy  Sessions. 

Planning 
Nov 20
Consent as the central legal framework 
- consent legal framework

Nov 21
Consent receipt.org  - working session

Nov 22


Panel Sessions List of names

Promotion Plan at the event
looking for sponsors
Promote the event, looking for activists who want to help make a privacy day campaign 
consentreceipt.org  - working on it.

People to search for

Looking for different people to have the 
Looking for lawyers who want to help with the consent legal framework - aimed to help open consent. 
Looking for designers to make this real!  
Looking for people that want to help codify consent notice law so its accessible by the masses. 

Projects
privacy day: to advocate campaign to get transparency over consent
provide everyone the ability to create consent receipt from registry, to then use to make a valid consent management request. 
a form can be made that people fill out to create the company information
can be linked to company api to auto fill the company
can make a request to see what the purpose of the consent is according to the policy at the time of last consent. 

